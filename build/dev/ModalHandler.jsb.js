import logging from 'node-logging-js';
import jsb from 'node-jsb';
import $ from 'jquery';

import 'bootstrap';

import './ModalHandler.scss';

class ModalHandler {
    constructor(element, options) {
        logging.applyLogging(this, 'ModalHandler');
        this.element = element;
        options = options || {};
        this.modal =  document.getElementById((options.modalHandlerModalId ||  'modalHandlerModal' ));
        this.deleteModal =  document.getElementById((options.modalHandlerDeleteModalId ||  'modalHandlerDeleteModal'));
        this.listRefreshUrl = options.listRefreshUrl;
        this.listContainer = document.getElementById((options.listRefreshContainerClass ||  'listContainer'));

        this.currentPageNumberContainer = document.getElementById((options.currentPageNumber || 'currentPageNumber'));
        if(this.currentPageNumberContainer) {
            this.currentPageNumber = this.currentPageNumberContainer.dataset.currentpage;
        } else {
            this.currentPageNumber = 1;
        }

        this.loader = this.loadSpinner();

        if(this.modal === null) {
            this.modal = this.loadDefaultModal();
        }

        if(this.deleteModal === null) {
            this.deleteModal = this.loadDefaultDeleteModal();
        }

        setTimeout(this.initializeEventListeners(), 0);
    }

    initializeEventListeners() {
        let self =this;
        self.element.addEventListener("click", function (e){
            let clicked = e.target;
            //When delete button is clicked show confirmation modal
            //set attribute data-deletebutton="true" on a clickable element
            if(clicked.dataset.deletebutton) {
                e.preventDefault();
                let formId = clicked.dataset.form;
                if (formId !== null) {
                    $(self.deleteModal).find('button[type="submit"]').attr('form', formId);
                    let action = document.getElementById(formId).action;
                    //add current page to delete form action
                    document.getElementById(formId).action = action+'?page='+self.currentPageNumber;
                    $(self.deleteModal).modal('show');
                }
                //for other "non submit" clicks
            } else if(e.target.href !== undefined) {
                e.preventDefault();
                self.sendRequest();
            }
        });

        //submiting forms
        self.element.addEventListener("submit", function (e){
            e.preventDefault();
            self.sendRequest(true);
        });

        //fix scroll after closing delete modal
        $(document).on('hidden.bs.modal', '.modal', function () {
            $('.modal:visible').length && $(document.body).addClass('modal-open');
        });

        //closing "alert" (displayed after saving or updating) popup
        $(self.modal).on('click', '.modal .alert-close-button', function () {
            $(this).closest('.modal').modal('hide');
        });

        $(this.deleteModal).on('show.bs.modal', function (event) {
            $(this).css('zIndex', '9999');
        })
    }

    sendRequest(isForm = false)
    {
        let self =this;
        let method = 'GET';
        let formData  = null;
        let source = $(this.element);
        let url;

        if(isForm) {
            method = source.attr('method');
            url = source.attr('action');
            formData = source.serialize()

            //set first page if its a new element
            if(source.data('isedit').length === 0){
                this.currentPageNumberContainer.dataset.currentpage = 1;
                this.currentPageNumber = 1;
            }
        } else {
            url = source.attr('href');
        }


        if(url) {
            $(self.modal).find('.modal-content').prepend(this.loader);
            $.ajax(
                {
                    url: url,
                    method: method,
                    data: formData,
                    success: function (response) {
                        if (source.find('input[name="_method"]').val() !== 'DELETE') {
                            $(self.modal).find('.modal-content').html(response);
                            jsb.applyBehaviour(self.modal);
                            $(self.modal).css('zIndex', '9999').modal('show');
                        }
                        self.refreshItemList();
                    }
                }
            );
        }
    }

    refreshItemList()
    {
        let self = this;
        if(this.listRefreshUrl) {
            if(this.currentPageNumberContainer) {
                this.currentPageNumber= this.currentPageNumberContainer.dataset.currentpage || 1;
            }

            this.listRefreshUrl = this.listRefreshUrl + '/' + this.currentPageNumber;
            $.ajax(
                {
                    url: this.listRefreshUrl,
                    method: 'GET',
                    success: function (response) {
                        $(self.listContainer).html(response);
                        jsb.applyBehaviour(self.listContainer);
                    }
                }
            );
        }
    }

    loadSpinner()
    {
        return '<div class="spiner-overflow d-flex justify-content-center">\n' +
            '<div class="spinner-border text-primary" role="status">\n' +
            '  <span class="sr-only">Loading...</span>\n' +
            '</div></div>';
    }

    loadDefaultModal()
    {
        return $('<div class="modal fade"\n' +
            '     id="modalHandlerModal"\n' +
            '     tabindex="-1"\n' +
            '     role="dialog"\n' +
            '     aria-labelledby="modalHandlerModalLabel"\n' +
            '     aria-hidden="true"\n' +
            '     data-backdrop="static"\n' +
            '>\n' +
            '    <div class="modal-dialog modal-xl" role="document">\n' +
            '        <div class="modal-content">\n' +
            '        </div>\n' +
            '    </div>\n' +
            '</div>')[0];
    }

    loadDefaultDeleteModal()
    {
        return $('<div class="modal fade" id="modalHandlerDeleteModal" tabindex="-1" role="dialog" aria-labelledby="modalHandlerDeleteModalLabel" aria-hidden="true">\n' +
            '    <div class="modal-dialog">\n' +
            '        <div class="modal-content">\n' +
            '            <div class="modal-header">\n' +
            '                <h3>Deleting item</h3>\n' +
            '            </div>\n' +
            '            <div class="modal-body">\n' +
            '                You`re about to delete this item. Are you sure ?\n' +
            '            </div>\n' +
            '\n' +
            '            <div class="modal-footer">\n' +
            '                <button type="button" class="btn btn-secondary" data-dismiss="modal">\n' +
            '                    Cancel\n' +
            '                </button>\n' +
            '\n' +
            '                <button\n' +
            '                        type="submit"\n' +
            '                        class="btn btn-danger"\n' +
            '                        form="#"\n' +
            '                >\n' +
            '                    Deleteeee\n' +
            '                </button>\n' +
            '            </div>\n' +
            '        </div>\n' +
            '    </div>\n' +
            '</div>')[0];
    }
}

jsb.registerHandler('ModalHandler', ModalHandler);
export default ModalHandler;

