fix-permissions:
	sudo chmod -R g+w $(CURDIR) -R
	sudo chown :www-data $(CURDIR) -R
	sudo chmod a+rwX $(CURDIR) -R
docker:
	docker-compose run --rm php-cli
front:
	docker-compose run --rm --service-ports node-cli
stop:
	docker-compose down
docker-clear-data:
	$(MAKE) fix-permissions
	rm -rf .docker/cache/*
	rm -rf .docker/data/*